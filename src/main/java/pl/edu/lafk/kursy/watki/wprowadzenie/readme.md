# Wprowadzenie do wielowątkowości

* Przetwarzanie współbieżne, równoległe a szeregowe
* Wywłaszczanie i jego brak
* Wątek a proces
* Modele realizacji wielowątkowości
* Priorytet wątku w Javie i w Systemie Operacyjnym
* Komunikacja między procesami a komunikacja między wątkami
* Semafor, mutex

## Przetwarzanie współbieżne, równoległe a szeregowe

*Równoległe*

* Wątek A `|----|`
* Wątek B `|----|`
* Wątek C `|----|`
* Wątek D `|----|`

*Współbieżne*

<pre>
* Wątek A |   - --   -|
* Wątek B |        -- -   -|
* Wątek C |---    -|
* Wątek D |    -       ---|
</pre>

*Szeregowe*

<pre>
* Wątek A |----|
* Wątek B |    ----|
* Wątek C |        ----|
* Wątek D |            ----|
</pre>


## Wywłaszczanie i jego brak

Koncept wywłaszczenia to odebranie zasobów wątkowi lub procesowi lub zadaniu by przekazać je innym.

W skrócie:

1. System z wieloma procesami robił proces za procesem
2. Procesy zrobiły się dłuższe i bardziej skomplikowane. Taki proces potrafił długo trwać.
3. GUI, jak np. Windows 95 - użytkownik przełącza się pomiędzy procesami co chwila.
4. Nalezy móc PRZERZUCIĆ procesor z procesu A na proces B i na inne i z powrotem.
 
Wieloprocesowe systemy musiały nagle obsługiwać **przełączanie kontekstu**. Należało udawać przed użytkownikiem, że "wszystko idzie do przodu".
Współbieżne przetwarzanie daje gwarancję, że COŚ idzie do przodu. Dodaj do tego **podział czasu** i mamy.

Proces (wątek też) dostaje procesor na **jakiś czas**, po upływie którego jest **wywłaszczany**. Może zostać wywłaszczony wcześniej. Może oddać procesor wcześniej.

## Wątek a proces

* `pstree`
* `less`
* `idea`
* `firefox`
* `bash` czy `zsh`

Zadanie? Program? Proces? Lekki proces? Wątek?

### Wielo...co?

1. Wielozadaniowość - czyli konieczność realizacji wielu zadań naraz. Przetwarzanie współbieżne na jednym wykonawcy.
2. Wieloprocesorowość - czyli więcej niż jeden procesor (a zatem wykonawca).
3. Wielordzeniowość - czyli 1 procesor - wielu wykonawców (rdzenie).
4. Wieloprocesowość - logiczne i od zawsze. Ale pytanie ile masz przełączeń kontekstu.
5. **Wielowątkowość**...

## Modele realizacji wielowątkowości

- Wiele wątków aplikacji do jednego wątku jądra
  - np. biblioteka `green threads` w systemie Solaris 2.
  - Stosowany w systemach nie posiadających wątków jądra.
- Jeden do jednego
  - wątek użytkownika - wątek jądra.
  - Przykłady: systemy Windows NT/2000/XP, OS/2, systemy z kernelem Linuxa.
- Wiele wątków użytkownika do wielu wątków jądra
  - multipleksowanie wątków aplikacji na mniejszą lub równą liczbę wątków jądra.
  - Pozwala systemowi operacyjnemu utworzyć dostateczną liczbę wątków jądra 
  - dobra współbieżność i dobra wydajność.
  - Często występuje poziom pośredni w postaci procesów lekkich (LWP), będących dla wątków użytkownika czymś w rodzaju wielowątkowych wirtualnych procesorów – z każdym LWP związany jest jeden wątek jądra, natomiast zwykły proces może składać się z jednego lub więcej LWP.
  - Przykłady: systemy Solaris 2, IRIX, HP-UX, Tru64 UNIX
- Pule wątków (ang. thread pools)
  - Wielo[proces|wątk]owość jest powszechnie stosowana w serwerach WWW (1 na żądanie).
  - Brak ograniczeń dotyczących wątków może doprowadzić do wyczerpania zasobów systemu, takich jak pamięć lub czas procesora.
  - Jednym z rozwiązań jest zastosowanie puli wątków–w chwili uruchomienia proces tworzy pewną liczbę wątków (pulę), które oczekują na zamówienia; kiedy nadchodzi zamówienie, wątek z puli jest budzony przez serwer, a po obsłużeniu zamówienia wraca do puli i czeka na kolejne zlecenie.
  - ☺ Zwykle łatwiej jest obsłużyć zamówienie za pomocą istniejącego wątku niż tworzyć do tego celu nowy wątek.
  - ☺ Pula wątków ogranicza liczbę wątków, co chroni przed wyczerpaniem zasobów systemowych, a także spadkiem wydajności systemu.

Więcej: https://slideplayer.pl/slide/419365/

## Priorytet wątku w Javie i w Systemie Operacyjnym

- Metoda `setPriority` umożliwia ustawienia priorytetu wątkowi.
- Priorytet określa jak szybko można "dopchać się" przed oblicze Planisty
- **Planista** to nowy koncept: to program rozporządzający czasem pracy procesora. 

## Komunikacja między procesami a komunikacja między wątkami

później

## Semafor, mutex

później
