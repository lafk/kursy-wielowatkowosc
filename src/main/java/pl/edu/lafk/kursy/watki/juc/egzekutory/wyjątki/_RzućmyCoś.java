package pl.edu.lafk.kursy.watki.juc.egzekutory.wyjątki;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.egzekutory.ZakończmyTo;

/**
 * Spróbujmy rzucić wyjątkiem w wątku. Co się stanie?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(ZakończmyTo.class)
@Następne(ObsługaRzucanego.class)
public class _RzućmyCoś {

    public static void main(String[] args) {
        new Thread(() -> {
            System.out.println("...");
            throw new RuntimeException("stało się coś poważnego");
        }).start();
    }
}
