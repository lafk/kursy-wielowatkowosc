package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Czemu taki dziwny rezultat?
 *
 * @see <a href="https://stackoverflow.com/q/30534164/999165">pytanie Jeeta na SO</a>
 * @author Jeet Parekh (najpewniej)
 */
@Poprzednie(Nazwy.class)
@Następne(DomyślneNazwy.class)
class Fabric extends Thread {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Fabric());
        Thread t2 = new Thread(new Fabric());
        Thread t3 = new Thread(new Fabric());
        t1.start();
        t2.start();
        t3.start();
    }

    public void run() {
        for(int i = 0; i < 2; i++)
            System.out.print(Thread.currentThread().getName() + " ");
    }
}
