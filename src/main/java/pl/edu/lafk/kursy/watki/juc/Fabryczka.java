package pl.edu.lafk.kursy.watki.juc;

import java.util.concurrent.ThreadFactory;

/**
 * Fabryczka wątków do egzekutorów, przyjmuje nazwę
 *
 * @author Tomasz @LAFK_pl Borek
 */
public class Fabryczka implements ThreadFactory {
    private static int nr = 0;
    private String nazwa;

    public Fabryczka(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, nazwa+"---nr---"+nr);
        nr++;
        return t;
    }
}
