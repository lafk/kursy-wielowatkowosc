package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.Fabryczka;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Nawet gdy mamy tylko kata, można go rzutować na usługi, jeśli użyliśmy Executors.
 * Ta klasa zwraca albo usługi albo TPE, który usługi implementuje.
 *
 * Zerknijcie też na końcowe linie. Jak widać, można wyłączać pule wątków!
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(KatDoUsług.class)
@Następne(NaCzas.class)
class Egzekutywa {
    Fabryczka f = new Fabryczka("Egzekutywa-czyli-Wroniec");
    Object ff = new Object();
    static int licznik = 0;

    public static void main(String[] args) {
        Egzekutywa egze = new Egzekutywa();
        Executor e = Executors.newFixedThreadPool(3,egze.f);
        e.execute(() -> {
            synchronized (egze.ff) {
                licznik++;
                System.out.println(licznik);
            }
        });
        ((ExecutorService) e).shutdown();
        try {
            ((ExecutorService) e).awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e1) {
            Thread.currentThread().interrupt();
        }
        System.out.println(((ExecutorService) e).isShutdown());
    }
}
