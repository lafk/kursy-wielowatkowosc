package pl.edu.lafk.kursy.watki.watek;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Różne sposoby robienia sekwencji
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(ThreadDeath.class)
@Następne(ThreadDeath.class)
class Sekwencje {

    static AtomicInteger i = new AtomicInteger(0);
    AtomicReference ar;


    static void plus() {
        i.getAndAdd(1);
    }

    static void minus() {
        i.getAndDecrement();
    }


    public static void main(String[] args) throws InterruptedException {
        final Thread plus = new Thread(() -> {
            for (int i = 0; i < 40_000; i++) {
                Sekwencje.plus();
            }
        }, "plus");
        plus.start();
        final Thread minus = new Thread(() -> {
            for (int i = 0; i < 40_000; i++) {
                Sekwencje.minus();
            }
        }, "minus");
        minus.start();

        plus.join();
        minus.join();
        System.out.println(Sekwencje.i);
    }
}
