package pl.edu.lafk.kursy.watki.grafika;
import java.awt.*;
import java.awt.event.*;

/**
 * Apka graficzna przeróbka z GH
 *
 * @author Tomasz @LAFK_pl Borek
 */

class Apka extends Frame implements ActionListener {

    private int x1=50,x2=50,x3=50;
    private Button b1,b2;

    private Thread t1,t2,t3;
    private ThreadGroup tg;

    private void pauseDrawing() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }


    public Apka() {
        setLayout(new FlowLayout());
        tg = new ThreadGroup("tg");
        Runnable r = () -> {
            while (true) {
                if (Thread.currentThread().equals(t1)) {
                    x1 += 2;
                    pauseDrawing();
                }
                if (Thread.currentThread().equals(t2)) {
                    x2 += 10;
                    pauseDrawing();
                }
                if (Thread.currentThread().equals(t3)) {
                    x3 += 5;
                    pauseDrawing();
                }
                repaint();
            }
        };
        t1 = new Thread(tg, r);
        t2 = new Thread(tg, r);
        t3 = new Thread(tg, r);

        b1 = new Button("Start");
        b2 = new Button("Stop");
        add(b1);
        add(b2);
        t1.start();
        t2.start();
        t3.start();

        b1.addActionListener(this);
        b2.addActionListener(this);

        setSize(500,500);
        setVisible(true);

        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent we)
            {
                dispose();
            }

        });
    }


    public static void main(String[] args) {
        new Apka();
    }


    public void paint(Graphics g)
    {
        malujOkrąg(g, Color.red, x1, 150);
        malujOkrąg(g, Color.green, x2, 350);
        malujOkrąg(g, Color.blue, x3, 250);

    }

    private void malujOkrąg(Graphics g, Color red, int x1, int i) {
        g.setColor(red);
        g.fillOval(x1, i, 50, 50);
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(b1))
        {
            tg.resume();
        }

        if(e.getSource().equals(b2))
        {
            tg.suspend();
        }
    }


}
