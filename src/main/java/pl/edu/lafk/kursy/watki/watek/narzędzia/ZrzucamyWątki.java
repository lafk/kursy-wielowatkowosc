package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;
import pl.edu.lafk.kursy.watki.watek.operacje._NajpierwAPotemB;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Arrays;


/**
 * Znajdź i wpisz tutaj sposoby na zrzucanie wątków razem z krótkim opisem (za, przeciw)
 *<ol>
 *     <li>Ctrl+Break - wymaga Intellij IDE, nie każda klawiatura ma Break.</li>
 *     <li>kodem, via getThreadInfo()</li>
 *     <li>?</li>
 *     <li>?</li>
 *     <li>?</li>
 *     <li>?</li>
 *     <li>?</li>
 *     <li>?</li>
 *</ol>
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(DomyślneNazwy.class)
@ZróbToSam
@Następne(MasowaProdukcjaWątków.class)
public class ZrzucamyWątki {

    public static void main(String[] args) {
        //TODO: zrzuć wątki na wiele sposobów

        kodem();
    }

    private static void kodem() {
        final ThreadMXBean wątkoweZiarno = ManagementFactory.getThreadMXBean();
        Arrays.stream(wątkoweZiarno
                .getThreadInfo(wątkoweZiarno.getAllThreadIds(), 100))
                .forEach(System.out::println);
    }
}
