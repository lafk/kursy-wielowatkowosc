package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.watek.operacje.LepszeSK;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Jak zmieniało się wołanie wątków w różnych Jawach?
 * Po co SingleThreadExecutor?
 * Kiedy i jak się to wykonuje?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(LepszeSK.class)
@Następne(UstawionyRozmiar.class)
public class _NowyWątekOdJawy5 {

    public static void main(String[] args) throws InterruptedException {
        Thread przedJawą5 = new Thread();
        przedJawą5.start();
        ExecutorService przedJawą8 = Executors.newSingleThreadExecutor();
        final Runnable komendaJava7 = new Runnable() {
            @Override
            public void run() {

            }
        };

        przedJawą8.execute(komendaJava7);
        Thread poJawie8 = new Thread(() -> {});
        poJawie8.start();
        System.out.println("===================== WITAMY ================");
        System.out.println("koniec pracy");
    }
}
