package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Wątek objawia nam prawdy prawdziwe i objawione. Jak dojdzie do sensu życia, to skończy.
 * W międzyczasie... będzie się produkować! Byle by robił to szybciej...
 *
 * @apiNote By tworzyć mędrca długobrodego, proponuję wartości: 300, 0, 100
 * @author Tomasz @LAFK_pl Borek
 */
class Mędrzec extends Thread {

    private final int ilość;
    private final int sufit;
    private final int podłoga;
    private int [] prawdy;

    Mędrzec() {
        this.ilość = 100;
        this.sufit = 60;
        this.podłoga = 20;
        prawdy = ThreadLocalRandom.current().ints(ilość, podłoga, sufit).toArray();
    }

    Mędrzec(int ilość, int sufit, int podłoga) {
        this.ilość = ilość;
        this.sufit = sufit;
        this.podłoga = podłoga;
        prawdy = ThreadLocalRandom.current().ints(ilość, podłoga, sufit).toArray();
    }

    @Override
    public void run() {
        super.run();
        int obecnie = 0;
        do {
            System.out.format("obecna prawda: %d \n", prawdy[obecnie]);
            obecnie++;
        } while (prawdy[obecnie] != 42);
        System.out.println("Objawiony został sens życia");
    }
}
