package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.wprowadzenie.start.StartujCzyBiegnij;

/**
 * Początki spania w wątki
 * eee... znaczy, GRANIA w wątki...
 *
 * Zabawcie się ilością tych milisekund przekazanych metodzie sleep().
 * Potem zwróćcie uwagę na linię 29.
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(StartujCzyBiegnij.class)
@Następne(Pobudka.class)
public class _Śpioch {
    public static void main(String[] args) throws InterruptedException {
        Thread śpioch = new Thread(() -> {
            try {
                Thread.sleep(1);
                System.out.println("odgłosy chrapania");
            } catch (InterruptedException e) {
                System.err.println("chwila kaszlu...");
            }
        },"śpioch");
        śpioch.start();
        śpioch.sleep(10);
        System.out.println("wątas główny skończył");

    }
}
