package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Jak zakończyć pracę egzekutora? Kiedy wykona się wrzucone do egzekutora zadanie? Czy może się ono... niewykonać?
 *
 * @see Komenda
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Oszczędne.class)
@Nurkujemy("execute")
@Następne(Egzekutywa.class)
class ExecuteInService {

    public static void main(String[] args) {
        ExecutorService es = Executors.newSingleThreadExecutor();
        // Komenda: klaska z przykładowymi zadaniami, wyekstrahowana dla klaru
        es.execute(Komenda.śpijPiątkęIKończ());
        System.out.println("===================== WITAMY ================");
        System.out.println("koniec pracy");
    }

}
