# Wielowątkowość w Java, mechanizmy niskopoziomowe i inne zagadnienia

* Kontrola cyklu życia wątku
* Parametry wątków
* Obsługa wyjątków w wątkach
* Monitor, `synchronized`  oraz `wait/notify` 
* Do tego z części poprzednich...

## Uzupełnione:

**Wprowadzenie do wielowątkowości** o:

* Wywłaszczanie i jego brak
* Priorytet wątku w Javie i w Systemie Operacyjnym
* Semafor, mutex

## Kontrola cyklu życia wątku
* Parametry wątków
* Obsługa wyjątków w wątkach
* Monitor, `synchronized`  oraz `wait/notify` 

## Lektura i przydatne sznurki

- http://jdk.java.net/jmc/ - skąd wziąć JMC
- https://visualvm.github.io/ - skąd wziąć JVisualVM