package pl.edu.lafk.kursy.watki;

/**
 * Holder
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Ucieczka {

    static class Holder {
        private int n;
        public Holder(int n) { this.n = n; }
        public void assertSanity() {
            if (n != n)
                throw new AssertionError("This statement is false.");
        }
    }

    static Holder h;

    public static void main(String[] args) {

        for (int i = 0; i < 100_000; i++) {
            h = new Holder(22);
//            new Thread(() -> {
//                h = new Holder(43);
//            }, "konstruktor").start();

            new Thread(() -> {
                h.assertSanity();
            }, "sprawdzacz").start();
        }
    }
}
