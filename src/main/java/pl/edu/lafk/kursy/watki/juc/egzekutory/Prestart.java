package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Prestartowanie wątków
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(WszystkieAPIWAkcji.class)
@Następne(PodstawaEgzekutorów.class)
class Prestart {

    public static void main(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(4);
        ((ThreadPoolExecutor)es).prestartAllCoreThreads();

//        Executors.newFixedThreadPool(2).
    }
}
