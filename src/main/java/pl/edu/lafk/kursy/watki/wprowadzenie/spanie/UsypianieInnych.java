package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;

/**
 * <ol>
 *     <li>Przeczytaj kod - co Twoim zdaniem się stanie gdy go odpalimy?</li>
 *     <li>Odpal ten wątek i zaobserwuj co się dzieje.</li>
 *     <li>Wywołaj Mędrca tak, by był długobrody (Parametry do konstruktora). Odpal raz jeszcze.</li>
 *     <li>Wytłumacz ten kod</li>
 * </ol>
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(MiliNano.class)
@ZróbToSam
@Następne(value=Draka.class, inspekcje=true)
class UsypianieInnych {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Wątek zaczyna pracę");
        Mędrzec m = new Mędrzec();
        m.start();
        m.sleep(5000);
        System.out.println("Wątek kończy pracę");
    }
}
