package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.time.LocalDateTime;
import java.util.concurrent.*;

/**
 * Wita świat i drukuje czas i pokazuje tworzenie i interwałowe wykonanie wątków.
 *
 * <ol>
 *     <li>Ile tutaj zadań się wykonuje?</li>
 *     <li>Dodaj własne zadanie i zerknij, czy przeplatanie idzie wg planu</li>
 * </ol>
 */
@Poprzednie(UstawionyRozmiar.class)
@Następne(EgzekutorPlusFabrykaWątków.class)
class WgRozkładu {

    public static void main(String[] args) {

        ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
        es.scheduleAtFixedRate(() -> System.out.println(LocalDateTime.now()),0,1,TimeUnit.SECONDS);
        es.scheduleAtFixedRate(() -> System.out.println("Witaj mój drogi, i nie bynajmniej nie świecie"),50,200,TimeUnit.MILLISECONDS);
        System.out.println("koniec pracy");
    }
}