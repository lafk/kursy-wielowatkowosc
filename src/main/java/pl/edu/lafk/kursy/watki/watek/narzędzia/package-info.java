/**
 * Okazja do poznania drugiej dobrej praktyki: że wątki należy nazywać.
 * Prezentacja kodu i narzędzi do zrzucania wątków i obsługi tychże.
 *
 * Poznajemy: ThreadInfo, ThreadFactory, setName, domyślne nazewnictwo anonimowych wątków, przekaz nazwy przez konstruktor.
 * Używamy: JVisualVM, JConsole, JMC, jcmd, jstack, jps, oraz kill.
 */
package pl.edu.lafk.kursy.watki.watek.narzędzia;