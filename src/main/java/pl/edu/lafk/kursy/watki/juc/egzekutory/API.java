package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.Fabryczka;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Skoro o API mowa, to popatrzmy na API dla egzekutorów. Jest go trochę, jak widać.
 * <ol>
 *     <li>Executor</li>
 *     <li>ExecutorService</li>
 *     <li>Executors</li>
 *     <li>XExecutorService</li>
 * </ol>
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(EgzekutorPlusFabrykaWątków.class)
@Nurkujemy("Executor i inni")
@Następne(KatDoUsług.class)
class API {
    ScheduledExecutorService ses =      Executors.newSingleThreadScheduledExecutor(new Fabryczka("czasomierz"));
    Executor rodzic =                   Executors.newSingleThreadScheduledExecutor(new Fabryczka("czasomierz"));
    ExecutorService serwis =            Executors.newSingleThreadScheduledExecutor(new Fabryczka("czasomierz"));
    ScheduledExecutorService serwis2 =  Executors.newSingleThreadScheduledExecutor(new Fabryczka("czasomierz"));


    ExecutorService dziecko =           Executors.newScheduledThreadPool(2, new Fabryczka("zegary"));
    ScheduledExecutorService  sesPula = Executors.newScheduledThreadPool(2, new Fabryczka("zegary"));

    public static void main(String[] args) {

    }
}

