package pl.edu.lafk.kursy.watki.juc.egzekutory;

/**
 * Prosta śpijPiątkęIKończ do wykonania w Egzekutorskich klasach, dla uniknięcia duplikacji kodu
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Komenda {

    static Runnable śpijPiątkęIKończ() {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5_000);
                    System.out.println("KOMENDA: KONIEC");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
    }

    static Runnable śpijWirujWBlokadzieDoPrzerwania() {
        return () -> {
            try {
                Thread.sleep(5_000);
                System.out.println("KOMENDA: KONIEC");
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
                while(!Thread.interrupted()){ System.out.println("KOMENDA: ."); }
        };
    }

    public static Runnable skrócićOGłowę() {
        return () -> System.out.println("Królowa Kier: skrócić go o głowę!");
    }
}
