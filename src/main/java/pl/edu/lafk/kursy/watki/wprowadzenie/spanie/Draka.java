package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;


import pl.edu.lafk.kursy.adnotacje.Kwiz;
import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;
import pl.edu.lafk.kursy.watki.watek.narzędzia.Main;

/**
 * <p>Dawno dawno temu, za górami, za lasami, królewiczowi spodobała się Śnieżka.</p>
 * <p>Wwkurzało go natomiast, że żyje ona z siedmioma krasnoludkami. No jeden to jeszcze, ale kuźwa SIEDMIU?!</p>
 * <p>Jak na królewicza przystało, ogarnął się, ulizał, ruszył w konkury, wyrwał laskę... i żyli długo i szczęśliwie...</p>
 *
 * <br/>
 * ale czy ktokolwiek kiedykolwiek pomyślał, co było dalej z krasnoludkami?
 * Wciel się w Gburka, Wesołka i Gapcia. Królewicz po ciężkiej pracy chce się wyspać? Nie na Waszej zmianie! Czas na zemstę!
 *
 * @implNote krasnoludki to Twoje wątki. Jak królewiczowi sen już przerwano, to nic nie rób. No, możesz się cicho zaśmiać. Ale nie pozwól królewiczowi spać!
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(UsypianieInnych.class)
@ZróbToSam
@Kwiz
@Następne(Main.class)
public class Draka {
    final Thread śpiącyKrólewicz = new Thread(new ŚpiącyKrólewicz(), "śpiący królewicz");

    public static void main(String[] args) {
        Draka draka = new Draka();
        draka.śpiącyKrólewicz.start();
        new Thread(złośliwyKrasnoludek(draka.śpiącyKrólewicz), "Gburek").start();
        new Thread(złośliwyKrasnoludek(draka.śpiącyKrólewicz), "Wesołek").start();
        new Thread(złośliwyKrasnoludek(draka.śpiącyKrólewicz), "Gapcio").start();
    }

    private static Runnable złośliwyKrasnoludek(Thread śpiącyKrólewicz) {
        return () -> {
            if (!śpiącyKrólewicz.isInterrupted()) {
                śpiącyKrólewicz.interrupt();
                System.out.println(Thread.currentThread().getName() + ": jam to, nie chwaląc się, sprawił");
            } else
                System.out.println(Thread.currentThread().getName() + ": hihihi");
        };
    }

}
