package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;


/**
 * @see Draka
 * @see <a href="https://www.yegor256.com/2015/10/20/interrupted-exception.html">Dobra obsługa przerwań, anglojęzyczny wpis</a>
 * @author Tomasz @LAFK_pl Borek
 */
class ŚpiącyKrólewicz implements Runnable {

    @Override
    public void run() {
        while (true) {
            System.out.println("hrrrrr fiiuuuuu");
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                System.err.println("Co, co, jak? Wyginęli? Przecież to nie były mamuty!");
            }
        }
    }
}
