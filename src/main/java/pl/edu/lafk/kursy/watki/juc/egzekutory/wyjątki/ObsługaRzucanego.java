package pl.edu.lafk.kursy.watki.juc.egzekutory.wyjątki;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(_RzućmyCoś.class)
@Następne(AWEgzekutorach.class)
class ObsługaRzucanego {

    public static void main(String[] args) {
        final Thread t = new Thread(() -> {
            System.out.println("...");
            throw new RuntimeException("stało się coś poważnego");
        });
        t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("och nie!");
            }
        });
        t.start();
    }

}
