package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.Fabryczka;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Czasem trzeba kogoś skrócić o głowę, nawet nie będąc Królową Kier. I co wtedy? Brać kata, czy katowskie usługi?
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(API.class)
@Następne(Egzekutywa.class)
class KatDoUsług {

    public static void main(String[] args) {
        Executor kat = Executors.newFixedThreadPool(3, new Fabryczka("kat"));
        ExecutorService katowskieUsługi = Executors.newFixedThreadPool(3, new Fabryczka("usługi katowskie, sp. z o.o."));

        kat.execute(Komenda.skrócićOGłowę());
        katowskieUsługi.submit(Komenda.skrócićOGłowę());
    }
}
