package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;
import pl.edu.lafk.kursy.watki.wprowadzenie.spanie.Draka;

/**
 * Ot, zwykły main z - nomen omen - metodą o tej samej nazwie.
 *<ol>
 *     <li>Uśpij "bieżący wątek" w tejże metodzie, na jakieś pół minuty proszę.</li>
 *     <li>Zrzuć wątki używając IDE (Ctrl+Break), przycisk migawki.</li>
 *     <li>Porozmawiajmy o nich chwilę.</li>
 *     <li>Dodaj dwa swoje wątki (nieważne jakie).</li>
 *     <li>Odpal program raz jeszcze i zrzuć wątki raz jeszcze. Wskaż swoje wątki.</li>
 *</ol>
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Draka.class)
@ZróbToSam
@Następne(Nazwy.class)
public class Main {

    public static void main(String[] args) throws InterruptedException {
        //TODO: uśpij mnie!
        Thread.sleep(30_000);
    }
}
