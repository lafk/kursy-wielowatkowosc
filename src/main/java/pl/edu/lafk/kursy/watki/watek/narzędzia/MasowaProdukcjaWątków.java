package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;
import pl.edu.lafk.kursy.watki.watek.operacje._NajpierwAPotemB;

/**
 * Są sytuacje, kiedy trza produkować wątki en masse, hurtowo, od szablonu.
 * W takiej sytuacji, pomoca jest fabryczka wątków.
 * <ol>
 *  <li>Wyprodukuj tuziny wątków demonicznych.</li>
 *  <li>Niech każdy zlicza ile w danym katalogu jest plików Java.</li>
 *  <li>Puść po jednym wątku na każdy katalog danego projektu.</li>
 * </ol>
 *
 * @see Fabryczka
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(ZrzucamyWątki.class)
@ZróbToSam
@Następne(_NajpierwAPotemB.class)
class MasowaProdukcjaWątków {

    // TODO: patrz Javadoc

}
