package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Wita świat i drukuje czas i pokazuje tworzenie wątków od szablonu.
 */
@Poprzednie(WgRozkładu.class)
@Następne(API.class)
class EgzekutorPlusFabrykaWątków {

    public static void main(String[] args) {

        ThreadFactory myThreadFactory = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "maleńkie wąciątko");
            }
        };
        ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor(myThreadFactory);
        es.scheduleAtFixedRate(() -> System.out.println(LocalDateTime.now()),0,1,TimeUnit.SECONDS);
        es.scheduleAtFixedRate(() -> System.out.println("Witaj mój drogi, i nie bynajmniej nie świecie"),50,200,TimeUnit.MILLISECONDS);
        System.out.println("koniec pracy");
    }
}