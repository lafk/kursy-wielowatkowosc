package pl.edu.lafk.kursy.watki.watek.narzędzia;

import java.util.concurrent.ThreadFactory;

/**
 * Fabryczka wątków demonicznych
 *
 * @author Tomasz @LAFK_pl Borek
 */
class Fabryczka implements ThreadFactory {
    private static int nr = 0;

    //TODO: konstruktor

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName("zFabryczki-nr" + nr);
        t.setDaemon(true);
        t.setPriority(2);
        nr++;
        return t;
    }
}