package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Jakie są domyślne nazwy wątków?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Fabric.class)
@Nurkujemy("konstruktor")
@Następne(ZrzucamyWątki.class)
class DomyślneNazwy {

    Thread t = new Thread();

}
