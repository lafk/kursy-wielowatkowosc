package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * Podstawa wszystkch egzekutorów - zerknijmy na to, co robi!
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Oszczędne.class)
@Nurkujemy("ThreadPoolExecutor")
@Następne(Dostosowany.class)
class PodstawaEgzekutorów {
    ThreadPoolExecutor tpeLinked = new ThreadPoolExecutor(2,4,1, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>(1));
    ThreadPoolExecutor synchroQ = new ThreadPoolExecutor(4,8,1, TimeUnit.SECONDS,
            new SynchronousQueue<>());
    ThreadPoolExecutor tpeOnArray = new ThreadPoolExecutor(2,4,1, TimeUnit.MINUTES,
            new ArrayBlockingQueue<>(1));


    public static void main(String[] args) {
        PodstawaEgzekutorów pe = new PodstawaEgzekutorów();
        IntStream.rangeClosed(1,100).forEach(i -> pe.tpeOnArray.execute(longtask()));
    }

    private static Runnable longtask() {
        return ()-> {
            System.out.println(Thread.currentThread().getName());
        };
    }
}
