package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.blokady._BlokadaWielowejściowa;
import pl.edu.lafk.kursy.watki.juc.egzekutory.wyjątki._RzućmyCoś;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Jak zakończyć pracę egzekutora? Kiedy wykona się wrzucone do egzekutora zadanie? Czy może się ono... niewykonać?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Dostosowany.class)
@Nurkujemy("shutdown, shutdownNow, terminate, awaitTermination?")
@Następne(_RzućmyCoś.class)
public class ZakończmyTo {


    public static void main(String[] args) throws InterruptedException {

        ExecutorService es = Executors.newSingleThreadExecutor();
        es.execute(Komenda.śpijPiątkęIKończ());
        es.execute(Komenda.śpijPiątkęIKończ());
        es.execute(Komenda.śpijWirujWBlokadzieDoPrzerwania());
        System.out.println("===================== WITAMY ================");
//        es.shutdownNow();
        es.shutdown();
        es.awaitTermination(20, TimeUnit.SECONDS);
        es.shutdownNow();
        System.out.println("koniec pracy");
    }

}
