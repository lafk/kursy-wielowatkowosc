package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.Fabryczka;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(NaCzas.class)
@Następne(WszystkieAPIWAkcji.class)
class Oszczędne {

    public static void main(String[] args) {
        Executors.newCachedThreadPool(new Fabryczka("oszczędne"));
    }

}
