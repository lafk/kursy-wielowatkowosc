package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.blokady._BlokadaWielowejściowa;

/**
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(PodstawaEgzekutorów.class)
@Następne(ZakończmyTo.class)
public class Dostosowany {
}
