package pl.edu.lafk.kursy.watki.watek.operacje;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Jedno z typowych pytań rekrutacyjnych: <b>co robi ten kod</b>?
 * Ja do niego dodam: <b>wskaż ewentualne błędy</b>.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(MasoweMejlowanie.class)
@Następne(WirującaBlokada.class)
class TypowaRekrutacja {

    public static void main(String[] args) {
        Thread t1 = new Thread(new EventThread("e1"));
        t1.start();
        Thread t2 = new Thread(new EventThread("e2"));
        t2.start();
        while (true) {
            try {
                t1.join();
                t2.join();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Udajmy, że to jakaś klasa z prawdziwego API...
     */
    private static class EventThread extends Thread {
        public EventThread(String nazwa) {
            super(nazwa);
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
