package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;

/**
 * Czas zaszaleć!
 * <ol>
 *     <li>stwórz pulę trzech wątków nazwanych "kat znaków".</li>
 *     <li>niech każdy "kat znaków" liczy znaki w jakimś pliku (sam wybierz jakim).</li>
 *     <li>stwórz pulę dwóch wątków nazwanych "usługi katowskie od osoby".</li>
 *     <li>niech każda usługa rozlosuje sto osób z GeneratoraOsób i sprawdzi, czy któraś z nich się powtarza.</li>
 *     <li>używając jednowątkowego egzekutora ogłoś upływ każdych 10 sekund jakie miną, aż do końca wszystkich pozostałych zadań</li>
 *     <li>używając jednowątkowego egzekutora zrzuć wątki i sprawdź ile usług katowskich zostało wynajętych</li>
 * </ol>
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Oszczędne.class)
@ZróbToSam
@Następne(Prestart.class)
class WszystkieAPIWAkcji {
}
