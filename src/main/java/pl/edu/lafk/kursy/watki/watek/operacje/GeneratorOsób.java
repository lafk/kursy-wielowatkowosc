package pl.edu.lafk.kursy.watki.watek.operacje;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Address;
import org.joda.time.DateTime;

import java.util.Locale;

/**
 * Owijka na JFairy, czyli wróżenie osób z fusów. Umożliwia otrzymanie
 * <li>mejla,</li>
 * <li>daty urodzenia,</li>
 * <li>adresu,</li>
 * <li>imienia i nazwiska,</li>
 * <li>producenta karty kredytowej,</li>
 * <li>daty ważności karty kredytowej,</li>
 * <li>NIPu</li>
 * ... Serwuje polskojęzyczne dane.
 *
 * @author Tomasz @LAFK_pl Borek
 */
class GeneratorOsób {
    Fairy wróżka = Fairy.create(Locale.forLanguageTag("pl"));

    String mejl() { return wróżka.person().getEmail(); }
    DateTime dataUrodzin() { return wróżka.person().getDateOfBirth(); }
    Address adres() { return wróżka.person().getAddress(); }
    String imie() { return wróżka.company().getName(); }
    String nip() { return wróżka.company().getVatIdentificationNumber(); }
    DateTime kartaWygasa() { return wróżka.creditCard().getExpiryDate(); }
    String karta() { return wróżka.creditCard().getVendor(); }
}