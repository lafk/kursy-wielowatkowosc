package pl.edu.lafk.kursy.watki.juc;

/**
 * Demonstracja bezpiecznego punktu, po tym jak pierwszy stał się ofiarą. On-Stack-Replacement. Kod demonstruje problem zawiśnięcia WMJa z racji na
 * czas do bezpiecznego (by zrzucić wątki) punktu (Time To Safe Point, TTSP).
 *
 * @see <a href="http://psy-lob-saw.blogspot.com/2016/02/wait-for-it-counteduncounted-loops.html">OSR i TTSP</a>
 * @author Nitsan Wakart
 */
class SafePoint {

    public static void main(String[] argc) throws InterruptedException {
        for (int i=0;i<100_000;i++) {
            countOdds(10);
        }
        Thread t = new Thread(() -> {
            long l = countOdds(Integer.MAX_VALUE);
            System.out.println("How Odd:" + l);
        });
        t.setDaemon(true);
        t.start();
        Thread.sleep(5000);
    }

    private static long countOdds(int limit) {
        long l = 0;
        for (int i = 0; i < limit; i++) {
            for (int j = 0; j < limit; j++) {
                if ((j & 1) == 1)
                    l++;
            }
        }
        return l;
    }
}
