package pl.edu.lafk.kursy.watki.watek.operacje;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Popularny rodzaj blokady, nie oddaje procesora. Od Javy 9 może mieć podpowiedzi by nie być optymalizowanym.
 *
 * @see <a href="https://vividcode.io/Java-9-Thread-onSpinWait/">Przykład z kodem</a>
 * @see <a href="https://stackoverflow.com/questions/44622384/onspinwait-method-of-thread-class-java-9">Do czego się kompiluje na Win32 i GNU/Linuxie</a>
 * @see <a href="http://openjdk.java.net/jeps/285">JEP 285: spin-wait hints</a>
 * @see Thread#onSpinWait()
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(TypowaRekrutacja.class)
@Następne(StrzeżonyBlok.class)
class WirującaBlokada {

    private boolean sukces;

    WirującaBlokada(boolean sukces) {
        this.sukces = sukces;
    }

    /**
     * Wirująca blokada (ang. spin-lock). Kręci prockiem, marnując cykle, ale nie oddaje procka, chyba, że ją JIT zoptymalizuje.
     */
    public void strzeżemySukcesu() {
        Thread.onSpinWait();
        while(!sukces) {}
        System.out.println("a seks plus pełna kasa, to wtedy sukces jest!");
    }

}
