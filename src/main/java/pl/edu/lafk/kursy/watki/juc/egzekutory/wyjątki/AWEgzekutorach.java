package pl.edu.lafk.kursy.watki.juc.egzekutory.wyjątki;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.*;

/**
 * Jak wyglada obsługa wyjątków w egzekutorach?
 * 
 * @author Peter Lawrey
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(ObsługaRzucanego.class)
@Następne(ThreadDeath.class)
class AWEgzekutorach {

    private ExecutorService kat = Executors.newSingleThreadExecutor();
    private Callable<Void> zadanko = () -> {
            System.out.println("** Start");
            Thread.sleep(2000);
            throw new IllegalStateException("Zły stan! Zły!");
        };
    private Future<Void> future = kat.submit(zadanko);


    public static void main(String[] args) {
        AWEgzekutorach awe = new AWEgzekutorach();
        try {
            awe.future.get(); // raises ExecutionException for any uncaught exception in child
        } catch (ExecutionException e) {
            System.out.println("** Wyjątek wykonania. Wołanie futureGet rzuciło, ponieważ w wątku poleciał: " + e.getCause());
            e.getCause().printStackTrace(System.err);
        } catch (InterruptedException e) {
            System.out.println("** Przerwanie w wątku!");
            e.getCause().printStackTrace((System.err));
        }
        awe.kat.execute(new FutureTask<>(awe.zadanko));
        awe.kat.shutdown();
        System.out.println("** Main zakończone.");

    }
}
