/**
 * Opisuje sposoby startowania wątków:
 * <ol>
 *     <li>klasyczne, przez klasę Thread</li>
 *     <li>klasyczne, przez interfejs Runnable</li>
 *     <li>klasyczne, via run()</li>
 *     <li>klasyczne, via start()</li>
 * </ol>
 *
 * @author Tomasz @LAFK_pl Borek
 */
package pl.edu.lafk.kursy.watki.wprowadzenie.start;