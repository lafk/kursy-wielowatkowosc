package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Jaka jest różnica między tymi wywołaniami?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Pobudka.class)
@Nurkujemy("sleep")
@Następne(value = UsypianieInnych.class, inspekcje = true)
class MiliNano {

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(1);
        Thread.sleep(0);
        Thread.sleep(0,1);
        Thread.sleep(1,234);
    }

}
