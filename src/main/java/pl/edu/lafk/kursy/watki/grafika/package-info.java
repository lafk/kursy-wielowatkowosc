/**
 * @see <a href="https://docs.oracle.com/javase/tutorial/uiswing/concurrency/index.html">Concurrency in Swing Sun/Oracle Trail</a>
 * @see <a href="https://www.ibm.com/developerworks/library/j-thread/index.html">Sekcja Threads and AWT/Swing</a>
 * @see <a href="https://stackoverflow.com/questions/11572726/java-awt-threads">Przykład z SO na ćwiczenie</a>
 * @see <a href="https://stackoverflow.com/questions/9507685/gui-threading-in-java-and-swingutilities">Przykład z SO na EDT + Runnable</a>
 *
 */
package pl.edu.lafk.kursy.watki.grafika;