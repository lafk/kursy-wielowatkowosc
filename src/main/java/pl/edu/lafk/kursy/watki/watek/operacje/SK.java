package pl.edu.lafk.kursy.watki.watek.operacje;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * SekcjaKrytyczna
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(StrzeżonyBlok.class)
@Następne(LepszeSK.class)
class SK {
    synchronized void a() {
        System.out.println("a " + Thread.currentThread().getName());
        b();
    }
    synchronized void b() {
        System.out.println("b " + Thread.currentThread().getName());
        c();
    }
    synchronized void c() {
        System.out.println("c " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        SK sk = new SK();
        new Thread(() -> sk.a(), "a").start();
        new Thread(() -> sk.c(), "c").start();
    }
}
