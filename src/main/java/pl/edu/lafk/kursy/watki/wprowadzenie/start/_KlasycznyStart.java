package pl.edu.lafk.kursy.watki.wprowadzenie.start;

import pl.edu.lafk.kursy.adnotacje.Następne;

import java.time.LocalDateTime;

/**
 * Wita świat i drukuje czas i pokazuje tworzenie wątków.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Następne(value = LambdowyStart.class, inspekcje = true)
class _KlasycznyStart {


    static class DoOdpalenia implements Runnable {
        @Override
        public void run() {
            System.out.println(LocalDateTime.now());
        }
    };

    static class MójWątek extends Thread {
        @Override
        public void run() {
            super.run();
            System.out.println("Witaj świecie");
        }
    }

    public static void main(String[] args) {
        Thread czasownik = new Thread(new DoOdpalenia(),"czasownik");
        Thread witacz = new MójWątek();
        czasownik.run();
        witacz.run();
        System.out.println("koniec pracy");
    }
}
