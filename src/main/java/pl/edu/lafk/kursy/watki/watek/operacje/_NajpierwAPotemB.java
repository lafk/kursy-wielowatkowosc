package pl.edu.lafk.kursy.watki.watek.operacje;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Address;
import org.joda.time.DateTime;
import pl.edu.lafk.kursy.adnotacje.Kwiz;
import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.watek.narzędzia.ZrzucamyWątki;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;


/**
 * Pokazuje pierwszy sposób synchronizacji wątków.
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(ZrzucamyWątki.class)
@Nurkujemy("join")
@Kwiz
@Następne(MasoweMejlowanie.class)
public class _NajpierwAPotemB {

    static Thread stoLiczb = new Thread(() ->
            new Random()
                    .ints(100)
                    .forEach(System.out::println)
            , "nowy");

    public static void main(String[] args) throws InterruptedException {
        System.out.println(pobierzMejleZInnegoSystemu());
        System.out.println("Start");
         stoLiczb.start();
        //TODO: co jeśli byśmy któryś z wątków uśpili?
         stoLiczb.join();
        System.out.println("Koniec");
    }

    /**
     * Udaje, że pobiera mejle z innego, odległego systemu i robi to powoli.
     */
    private static List<String> pobierzMejleZInnegoSystemu() {
        GeneratorOsób go = new GeneratorOsób();
        return Arrays.asList(go.mejl(), go.mejl(), go.mejl(), go.mejl());
    }
}
