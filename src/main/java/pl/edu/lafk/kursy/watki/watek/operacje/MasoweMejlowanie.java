package pl.edu.lafk.kursy.watki.watek.operacje;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;

/**
 * <li>Niech wątek główny wylosuje liczbę.</li>
 * <li>Dla wylosowanej liczby niech zostanie pobrane 'z bazy' (wróżką) tyle mejli,</li>
 * <li>każdy mejl jest pobierany osobnym wątkiem.</li>
 * <li>Jak tylko adres mejlowy spływa, niech osobny wątek udaje, że wysyła mejla</li>
 * <li>Tylko metody poznane dotąd mogą być użyte!</li>
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(_NajpierwAPotemB.class)
@ZróbToSam
@Następne(TypowaRekrutacja.class)
class MasoweMejlowanie {


}
