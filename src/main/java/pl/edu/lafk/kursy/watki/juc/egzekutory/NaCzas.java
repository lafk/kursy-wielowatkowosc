package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Nurkujemy;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.Fabryczka;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Są cztery istotne API w planistach-egzekutorach (nieważne ile te mają wątków)
 * <ol>
 *     <li>schedule(z Runnable lub Callable)</li>
 *     <li>scheduleWithFixedDelay - opóźnia odpalenie o X</li>
 *     <li>scheduleAtFixedRate - odpala co interwał</li>
 * </ol>
 * @see java.util.concurrent.TimeUnit - jak już w temacie API, to zerknijmy na tego BARDZO przydatnego enuma
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Egzekutywa.class)
@Nurkujemy("Executor i inni")
@Następne(Oszczędne.class)
class NaCzas {
    ScheduledExecutorService ses     = Executors.newSingleThreadScheduledExecutor(
            new Fabryczka("czasomierz"));
    ScheduledExecutorService sesPula = Executors.newScheduledThreadPool(
            2, new Fabryczka("zegary"));

    public static void main(String[] args) {

    }
}

