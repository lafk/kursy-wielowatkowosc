/**
 * Pakiet demonstruje usypianie i budzenie wątków.
 *
 * Wprowadza pierwszy wyjątek: {@code InterruptedException}, zrzut wątków (z IDE, ale nadal), pobieżnie omawia spanie, przerwania jak i sam zrzut.
 * <br>
 * Końcowe ćwiczenie ({@code Draka}), umożliwia zerknięcie na źle zrobioną obsługę przerwania i omówienie dobrej.
 *
 * @see <a href="https://www.yegor256.com/2015/10/20/interrupted-exception.html">Dobra obsługa przerwań, anglojęzyczny wpis</a>
 * @see java.lang.InterruptedException
 * @author Tomasz @LAFK_pl Borek
 */
package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;