/**
 * ## Wprowadzenie do wielowątkowości
 *
 * * Przetwarzanie współbieżne, równoległe a szeregowe
 * * Wywłaszczanie i jego brak
 * * Wątek a proces
 * * Modele realizacji wielowątkowości
 * * Priorytet wątku w Javie i w Systemie Operacyjnym
 * * Komunikacja między procesami a komunikacja między wątkami
 * * Semafor, mutex
 *
 * Niemal zupełnie teoretyczny i systemowy pakiet.
 * Na chwilę obecną, kod w Jawie to tylko pakiety start i spanie.
 *
 * @see pl.edu.lafk.kursy.watki.wprowadzenie.start
 * @see pl.edu.lafk.kursy.watki.wprowadzenie.spanie
 * @author Tomasz @LAFK_pl Borek
 */
package pl.edu.lafk.kursy.watki.wprowadzenie;