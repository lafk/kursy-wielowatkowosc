package pl.edu.lafk.kursy.watki.wprowadzenie.start;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Pokazuje relację klas anonimowych z lambdami przy biegnących wątkach.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(_KlasycznyStart.class)
@Następne(StartujCzyBiegnij.class)
class LambdowyStart {
    public static void main(String[] args) {
        System.out.println("3...");
        new Thread(() -> System.out.println("metoda run() po raz pierwszy"));
        System.out.println("2...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("metoda run() po raz drugi");
            }
        }).run();
        System.out.println("1...");
        new Thread(() -> System.out.println("metoda run() po raz trzeci")).run();
        System.out.println("Start!");
    }
}
