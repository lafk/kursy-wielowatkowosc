package pl.edu.lafk.kursy.watki.wprowadzenie.spanie;


import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.adnotacje.ZróbToSam;


/**
 * Odpal tę klase kilkanaście razy.
 * Następnie zrób to w terminalu, np. pętlą for.
 * Porównaj róznice.
 *
 * @see <a href="https://youtrack.jetbrains.com/issue/IDEA-70016">IDEA bug 70016 from 2011</a>
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(_Śpioch.class)
@ZróbToSam
@Następne(MiliNano.class)
class Pobudka {

    static Thread śpioch = new Thread(() -> {
        try {
            System.out.println("odgłosy chrapania");
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.err.println("chwila kaszlu...");
        }
    },"śpioch");

    public static void main(String[] args) {
        śpioch.start();
        śpioch.interrupt();
    }

}
