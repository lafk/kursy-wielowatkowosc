package pl.edu.lafk.kursy.watki.watek.narzędzia;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.ThreadFactory;


/**
 * Nazywanie wątków to ŚWIETNA praktyka. Wiele narzędzi do logowania wypluwa komunikaty z nazwą wątku.
 * Dobrze nazwany wątek oszczędza czas i pozwala się zlokalizować w kodzie w try miga.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Main.class)
@Następne(Fabric.class)
class Nazwy {
    public static void main(String[] args) {
        Runnable śpij30s = () -> {
            try {
                Thread.sleep(3_000);
                System.out.println(Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        new Thread(śpij30s).start();

        // konstruktorem
        new Thread(śpij30s, "30s snu").start();

        // via setName
        Thread t = new Thread(śpij30s, "30s snu konstruktor");
        t.setName("30s snu - set");
        t.start();

        // z fabryki
        // TODO: przyjrzyj się jak ten kod wygląda i zastąp go lambdą
        Thread zFabryki = new ThreadFactory() {

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "moja fabryczka wątusiów - 30s snu");
            }
        }.newThread(śpij30s);
        zFabryki.start();
        ThreadFactory tf = r -> new Thread(r, "nazwaWątkuFabrycznegoLambdowego");
        tf.newThread(śpij30s).start();

        // przeciążeniem klasy i wołaniem konstruktora

        class MójWątek extends Thread {

            private String nazwa;

            MójWątek(String nazwa) {
                super(nazwa);
            }

            @Override
            public void run() {
                System.out.println("Wątek " + Thread.currentThread().getName() + " melduje się");
                try {
                    Thread.sleep(30_000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    // Tak, jest to ostatni kod w tej metodzie, ale NWP, jak ktoś doda kod po tym.
                    return;
                }
            }
        }
        new MójWątek("jedwabisty").start();
    }
}
