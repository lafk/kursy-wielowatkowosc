package pl.edu.lafk.kursy.watki.kwiz;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Testuje znajomość API dotychczas poznanego.
 * @author Tomasz @LAFK_pl Borek
 */
class Kwiz {

    Kwiz(String kontekst) {
        System.out.println(kontekst);
    }

    public static void main(String[] args) throws IOException {
        Kwiz wątki = new Kwiz("Co robi dane API? Na co uważać?");
        wątki.odpytuj(wątki.wczytaj("threads.txt"));

        Kwiz wyjątki = new Kwiz("Kiedy ten wyjątek może polecieć? Jak obsłużyć?");
        wyjątki.odpytuj(wyjątki.wczytaj("exceptions.txt"));
    }

    private void odpytuj(Stream<String> linie) {
        linie.forEach(s -> {
            System.out.println(s);
            try {
                Thread.sleep(30_000);
            } catch (InterruptedException e) {
                System.err.println("Przerwanie");

            }
        });
    }

    private Stream<String> wczytaj(String plik) throws IOException {
        return Files.lines(Paths.get("src/main/resources/"+plik));
    }
}
