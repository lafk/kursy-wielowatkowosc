package pl.edu.lafk.kursy.watki.wprowadzenie.start;

import pl.edu.lafk.kursy.adnotacje.Kwiz;
import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.wprowadzenie.spanie._Śpioch;

import java.util.stream.IntStream;

/**
 * Lepiej kazać wątkom startować czy biec? Oto jest pytanie.
 * Jak zamienię dwie pierwsze linie miejscami, co się stanie? Jakiś efekt?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(LambdowyStart.class)
@Kwiz
@Następne(_Śpioch.class)
public class StartujCzyBiegnij {
    public static void main(String[] args) {
        IntStream.rangeClosed(1,10).forEach(System.out::println);
        new Thread(() -> System.out.println("beta")).start();
        new Thread(() -> System.out.println("alfa")).run();
    }
}
