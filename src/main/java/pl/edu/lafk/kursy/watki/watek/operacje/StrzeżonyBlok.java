package pl.edu.lafk.kursy.watki.watek.operacje;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.NotThreadSafe;
import net.jcip.annotations.ThreadSafe;
import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

/**
 * Przykład idiomu Javy, wait w pętli w synchronizacji, ang. GuardedBlock.
 *
 * @see <a href="https://docs.oracle.com/javase/tutorial/essential/concurrency/guardmeth.html">Oracle Java Trail - Guarded Block</a>
 * @JCIP rozdział 14
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(WirującaBlokada.class)
@Następne(SK.class)
@NotThreadSafe     // gdyby nie main, to owszem
class StrzeżonyBlok {

    @GuardedBy("this")
    private boolean sukces;

    StrzeżonyBlok(boolean sukces) {
        this.sukces = sukces;
    }

    public static void main(String[] args) {
        StrzeżonyBlok sb = new StrzeżonyBlok(false);
        Runnable r = sb::strzeżemySukcesu;
        Runnable ok = sb::jestSukces;
        new Thread(r, "garda").start();
        new Thread(ok, "nota").start();
    }

    /**
     * Wirująca blokada (ang. spin-lock) jako idiom Javy. Pętla raz przechodzi po zdarzeniu, które może NIE być zdarzeniem na które czekamy.
     */
    synchronized void strzeżemySukcesu() {
        while(!sukces) {
            try {
                System.out.println("czeka");
                wait();
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
        System.out.println("Sukces!");
    }


    synchronized void jestSukces() {
        sukces = true;
        notifyAll();
    }
}
