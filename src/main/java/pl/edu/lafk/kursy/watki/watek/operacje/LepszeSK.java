package pl.edu.lafk.kursy.watki.watek.operacje;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.egzekutory._NowyWątekOdJawy5;

/**
 * Sekcja Krytyczna z dedykowanymi a nie domyślnymi monitorami.
 * Takie monitory nie są częścią kontraktu publicznego czy pakietowego API, to plus.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(SK.class)
@Następne(_NowyWątekOdJawy5.class)
public class LepszeSK {
    private Object lockForC = new Object();
    private Object lockForA = new Object();

    void a() {
        synchronized(lockForA) {
            System.out.println("a " + Thread.currentThread().getName());
            b();
        }
    }
    void b() {
        synchronized (lockForA) {
            System.out.println("b " + Thread.currentThread().getName());
            c();
        }
    }
    void c() {
        synchronized (lockForC) {
            System.out.println("c " + Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {
        LepszeSK sk = new LepszeSK();
        new Thread(sk::c, "c").start();
        new Thread(sk::a, "a").start();
    }
}
