package pl.edu.lafk.kursy.watki.juc.blokady;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;
import pl.edu.lafk.kursy.watki.juc.egzekutory.Dostosowany;

/**
 * Synchronizacja klasyczna, wbudowana w Javę, ma opcję wielu wejść (dla jednego wątku). Oto jej ekwiwalent z pakietu juc: ReentrantLock, pierwsza z blokad z tego pakietu jaką poznacie.
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(Dostosowany.class)
@Następne(ThreadDeath.class)
public class _BlokadaWielowejściowa {
}
