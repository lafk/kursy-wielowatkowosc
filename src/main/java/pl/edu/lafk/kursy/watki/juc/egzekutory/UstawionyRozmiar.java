package pl.edu.lafk.kursy.watki.juc.egzekutory;

import pl.edu.lafk.kursy.adnotacje.Następne;
import pl.edu.lafk.kursy.adnotacje.Poprzednie;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Dobry, czy zły pomysł?
 *
 * @author Tomasz @LAFK_pl Borek
 */
@Poprzednie(_NowyWątekOdJawy5.class)
@Następne(WgRozkładu.class)
class UstawionyRozmiar {
    ExecutorService es = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors()-2);
}
