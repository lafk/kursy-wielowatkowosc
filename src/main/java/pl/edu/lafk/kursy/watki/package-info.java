/**
 * Pakiet <code>nazwa</code> - opis zawartości:
 * <ol>
 *     <li><code>start</code> - opisuje sposoby startowania wątków.</li>
 * </ol>
 *
 *
 * Klasy kwizowe zawierają testowe pytania.
 * @author Tomasz @LAFK_pl Borek
 */
package pl.edu.lafk.kursy.watki;