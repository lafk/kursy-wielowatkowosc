# Program

- Zapoznanie się
- Wielowątkowość?
- W Jawie?
- Problemy?
- Wzorce?
- Nowości?
- Optymalizacje?

## Zapoznanie się

- http://www.lafk.pl
- Sages
- Wy?

## Wprowadzenie do wielowątkowości

* Przetwarzanie współbieżne, równoległe a szeregowe
* Wywłaszczanie i jego brak P
* Wątek a proces
* Modele realizacji wielowątkowości
* Priorytet wątku w Jawie i w Systemie Operacyjnym P
* Komunikacja między procesami a komunikacja między wątkami P
* Semafor, mutex P

## Wielowątkowość w Java, mechanizmy niskopoziomowe i inne zagadnienia

* Kontrola cyklu życia wątku
* Parametry wątków
* Grupowanie wątków
* Obsługa wyjątków w wątkach
* Monitor, `synchronized`  oraz `wait/notify`
* `ThreadLocal` storage
* `Timer`
* Startowanie nowych procesów
* Wątki w Swing i AWT
* Concurrent Mark-Sweep GC
* Bariery pamięci i model pamięci Java
* `volatile`, `final`, `lazySet` i `CAS`

## Problemy w programowaniu wielowątkowym

* Przykładowe struktury programu wielowątkowego
* Wyścig, operacje atomowe
* Zakleszczenie i zbytnia uprzejmość (ang. _livelock_)
* Zagłodzenie i odwracanie priorytetu (ang. _Priority Inversion_)
* Algorytm Dekkera

## Wzorce projektowe dla programów wielowątkowych

* Wzorce synchronizacji
* Wzorce współbieżności
* Wzorce inicjalizacji
* Wzorce obsługi zdarzeń

## Omówienie nowości związanych ze współbieżnością (Java 7,8,9)

* Klasy zmiennych atomowych i operacje atomowe
* Interfejsy blokad – `Lock`, `ReadWriteLock`
* `Condition`
* Kolejki blokujące – `BlockingQueue`
* Kolekcje bezpieczne wątkowo
* Synchronizatory
* Zadania, obietnice przyszłości
* Współbieżne wykonywanie zadań
* `ForkJoin`

## Optymalizacja rozwiązań współbieżnych

* Źródła spadku wydajności we współbieżności.
* Redukcja rywalizacji o blokadę
* Prawo Amdahla
